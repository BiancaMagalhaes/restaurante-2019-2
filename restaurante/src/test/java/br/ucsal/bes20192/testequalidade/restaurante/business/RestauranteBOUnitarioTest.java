package br.ucsal.bes20192.testequalidade.restaurante.business;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import br.ucsal.bes20192.testequalidade.restaurante.exception.MesaOcupadaException;
import br.ucsal.bes20192.testequalidade.restaurante.exception.RegistroNaoEncontrado;
import br.ucsal.bes20192.testequalidade.restaurante.builder.MesaBuilder;

@RunWith(MockitoJUnitRunner.class)
public class RestauranteBOUnitarioTest {

	@Mock
	RestauranteBO restauranteBO;
	
	/**
	 * Método a ser testado: public void abrirComanda(Integer
	 * numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException. Verificar
	 * se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * Crie um builder para instanciar a classe Mesa.
	 */
	
	@Before
	public void previusTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void abrirComandaMesaLivre() throws RegistroNaoEncontrado, MesaOcupadaException{
		MesaBuilder mesaBuilder = new MesaBuilder();
		restauranteBO.abrirComanda(mesaBuilder.comNumero(MesaBuilder.DEFAULT_NUMERO).build().getNumero());
	}
}
