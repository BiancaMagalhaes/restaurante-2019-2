package br.ucsal.bes20192.testequalidade.restaurante.builder;

import br.ucsal.bes20192.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20192.testequalidade.restaurante.domain.Mesa;

public class MesaBuilder {
	
	public static final SituacaoMesaEnum DEFAULT_SITUACAO = SituacaoMesaEnum.LIVRE;
	public static final Integer DEFAULT_NUMERO = 10;
	public static final Integer DEFAULT_CAPACIDADE = 5;
	
	private SituacaoMesaEnum situacao = DEFAULT_SITUACAO;
	private Integer numero = DEFAULT_NUMERO;
	private Integer capacidade = DEFAULT_CAPACIDADE;
	
	public MesaBuilder() {}
	
	public static MesaBuilder Mesa() {
		return new MesaBuilder();
	}
	
	public MesaBuilder comSituacao(SituacaoMesaEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public MesaBuilder comNumero(Integer numero) {
		this.numero = numero;
		return this;
	}
	
	public MesaBuilder comCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
		return this;
	}
	
	public static MesaBuilder but() {
		return MesaBuilder.Mesa().comSituacao(SituacaoMesaEnum.LIVRE).comNumero(10).comCapacidade(5);
	}
	
	public Mesa build() {
		Mesa mesa = new Mesa();
		
		mesa.setSituacao(situacao);
		mesa.setCapacidade(capacidade);
		mesa.setNumero(numero);
		
		return mesa;
	}

}
